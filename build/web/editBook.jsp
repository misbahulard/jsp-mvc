<%-- 
    Document   : editBook
    Created on : Apr 6, 2017, 10:58:29 AM
    Author     : misbahul
--%>

<%@page import="com.misbahulard.model.BookModel"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Buku Kita</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
        <link rel="stylesheet" href="css/style.css">
    </head>
    <body>
        <nav class="navbar navbar-toggleable-md navbar-inverse bg-primary">
            <div class="container">
                <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                  <span class="navbar-toggler-icon"></span>
                </button>
                <a class="navbar-brand" href="index.jsp">Buku Kita</a>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                  <ul class="navbar-nav mr-auto">
                    <li class="nav-item active">
                      <a class="nav-link" href="index.jsp">Home <span class="sr-only">(current)</span></a>
                    </li>
                  </ul>
                  <form class="form-inline my-2 my-lg-0">
                    <input class="form-control mr-sm-2" type="text" placeholder="Search">
                    <button class="btn btn-success my-2 my-sm-0" type="submit">Search</button>
                  </form>
                </div>
            </div>
        </nav>
        <%
            BookModel bookModel = (BookModel)request.getAttribute("book");
            
            if (bookModel != null) {
                %>
                <div class="container">
                    <h1>Edit Books</h1>
                    <hr>
                    <form action="EditBook" method="post">
                        <input type="hidden" name="id" value="<%= bookModel.getIsbn() %>">
                        <div class="form-group">
                            <label for="isbn">ISBN:</label>
                            <input class="form-control" type="text" name="isbn" id="isbn" value="<%= bookModel.getIsbn() %>"/>
                        </div>

                        <div class="form-group">
                            <label for="title">Title:</label>
                            <input class="form-control" type="text" name="title" id="title" value="<%= bookModel.getTitle() %>" />
                        </div>
                        <div class="form-group">
                            <label for="author">Author:</label>
                            <input class="form-control" type="text" name="author" id="author" value="<%= bookModel.getAuthor()%>"/>
                        </div>
                        <div class="form-group">
                            <label for="year">Year:</label>
                            <input class="form-control" type="text" name="year" id="year" value="<%= bookModel.getYear()%>"/>
                        </div>
                        <div class="form-group">
                            <label for="price">Price:</label>
                            <input class="form-control" type="text" name="price" id="price" value="<%= bookModel.getPrice()%>"/>
                        </div>

                        <div class="form-group clearfix">
                            <input class="btn btn-primary float-right" type="submit" value="Submit" />
                        </div>
                    </form>
                </div>

                <%
            }
        %>
        <footer>
            <hr>
            <div class="container">
                &copy; Buku Kita 2017
            </div>
        </footer>
    </body>
</html>
