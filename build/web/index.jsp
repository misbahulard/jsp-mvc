<%-- 
    Document   : index
    Created on : May 10, 2017, 3:09:18 AM
    Author     : misba
--%>

<%@page import="com.misbahulard.model.BookModel"%>
<%@page import="java.util.ArrayList" %>
<jsp:useBean id="db" class="com.misbahulard.model.DAOconnection" scope="page" />
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Buku Kita</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
        <link rel="stylesheet" href="css/style.css">
    </head>
    <body>
        <nav class="navbar navbar-toggleable-md navbar-inverse bg-primary">
            <div class="container">
                <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                  <span class="navbar-toggler-icon"></span>
                </button>
                <a class="navbar-brand" href="index.jsp">Buku Kita</a>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                  <ul class="navbar-nav mr-auto">
                    <li class="nav-item active">
                      <a class="nav-link" href="index.jsp">Home <span class="sr-only">(current)</span></a>
                    </li>
                  </ul>
                  <form class="form-inline my-2 my-lg-0">
                    <input class="form-control mr-sm-2" type="text" placeholder="Search">
                    <button class="btn btn-success my-2 my-sm-0" type="submit">Search</button>
                  </form>
                </div>
            </div>
        </nav>
        
        <div class="container">
        
            <div class="row">
                <div class="col-md-7">
                    <h1>Book List</h1>
                    <hr>
                    <% 
                        boolean isSuccess = db.createConnection();

                        if (isSuccess) {
                            ArrayList<BookModel> dataList = db.showData();

                            if (dataList != null) {
                                out.print(
                                    "<table class='table table-striped table-sm'>" +
                                        "<thead class='bg-primary text-white'>" +
                                            "<tr>" +
                                                "<th> ISBN </th>" +
                                                "<th> Title </th>" +
                                                "<th> Author </th>" +
                                                "<th> Year </th>" +
                                                "<th> Price </th>" +
                                                "<th colspan=2> Action </th>" +
                                            "</tr>" +
                                        "</thead>"
                                );

                                for (BookModel model : dataList) {
                                    out.print(
                                        "<tr>" +
                                            "<td>" + model.getIsbn()+ "</td>" +
                                            "<td>" + model.getTitle()+ "</td>" +
                                            "<td>" + model.getAuthor()+ "</td>" +
                                            "<td>" + model.getYear()+ "</td>" +
                                            "<td>" + model.getPrice()+ "</td>" +
                                            "<td> <a class='btn btn-success' href='EditBook?isbn=" + model.getIsbn() +"'>Edit</a></td>" +
                                            "<td> <a class='btn btn-danger' href='DeleteBook?isbn=" + model.getIsbn() +"'>Delete</a></td>" +
                                        "</tr>"
                                    );
                                }
                                    out.println("</table>");
                            }
                        }
                    %>
                </div>
                <div class="col-md-5">
                    <h1>New Book</h1>
                    <hr>
                    <form action="AddBook" method="post">
                        <div class="form-group">
                            <label for="isbn">ISBN:</label>
                            <input class="form-control" type="text" name="isbn" id="isbn" />
                        </div>

                        <div class="form-group">
                            <label for="title">Title:</label>
                            <input class="form-control" type="text" name="title" id="isbn" />
                        </div>
                        <div class="form-group">
                            <label for="author">Author:</label>
                            <input class="form-control" type="text" name="author" id="author" />
                        </div>
                        <div class="form-group">
                            <label for="year">Year:</label>
                            <input class="form-control" type="text" name="year" id="year" />
                        </div>
                        <div class="form-group">
                            <label for="price">Price:</label>
                            <input class="form-control" type="text" name="price" id="price" />
                        </div>

                        <div class="form-group">
                            <input class="btn btn-primary float-right" type="submit" value="Submit" />
                        </div>
                    </form>
                </div>
            </div>
        </div>
                
        <footer>
            <hr>
            <div class="container">
                &copy; Buku Kita 2017
            </div>
        </footer>
    </body>
</html>
