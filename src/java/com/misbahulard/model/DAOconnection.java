package com.misbahulard.model;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author misbahul
 */

import java.sql.*;
import java.util.ArrayList;

public class DAOconnection {

    private Connection connection;
    
    public DAOconnection(){
        try {
            Class.forName("com.mysql.jdbc.Driver");
            
            System.out.println("Driver create uccessfull");
        } catch (ClassNotFoundException e) {
            System.out.println("Where is your mysql jdbc driver?");
            e.printStackTrace();
        }
        
        connection = null;
    }
    
    public boolean createConnection() {
        String url = "jdbc:mysql://localhost:3306/db_book_jsp";
        String username = "root";
        String password = "";
        
        boolean isSuccess = true;
        
        try {
            connection = DriverManager.getConnection(url, username, password);
            
            System.out.println("Connection Successful");
        } catch (SQLException e){
            System.out.println("Connection failed! Check output console");
            e.printStackTrace();
            
            isSuccess = false;
        }
        
        return isSuccess;
    }
    
    public Statement createStatement(){
        Statement statement = null;
        
        try {
            statement = connection.createStatement();
        } catch (SQLException e) {
            System.out.println("Failed to create statement");
            e.printStackTrace();
        }
        
        return statement;
    }
    
    public ArrayList<BookModel> showData() {
        Statement statement = createStatement();
        ArrayList<BookModel> tempList = null;
        
        if (statement != null) {
            String sql = "SELECT * FROM book";
            ResultSet resultSet = null;
            
            try {
                resultSet = statement.executeQuery(sql);
            } catch (SQLException e) {
                System.out.println("Failed to execute query");
                e.printStackTrace();
            }
            
            if (resultSet != null) {
                tempList = new ArrayList<>();
                
                try {
                    while(resultSet.next()){
                        BookModel bookModel = new BookModel();
                        
                        bookModel.setIsbn(resultSet.getInt(1));
                        bookModel.setTitle(resultSet.getString(2));
                        bookModel.setAuthor(resultSet.getString(3));
                        bookModel.setYear(resultSet.getString(4));
                        bookModel.setPrice(resultSet.getInt(5));
                        
                        tempList.add(bookModel);
                    }
                } catch (SQLException e) {
                    System.out.println("Failed to show result data..");
                    e.printStackTrace();
                }
            }
        }
        
        return tempList;
    }
    
    public BookModel showDataByIsbn(int isbn) {
        Statement statement = createStatement();
        BookModel bookModel = new BookModel();
        
        if (statement != null) {
            String sql = "SELECT * FROM book where isbn = " + isbn;
            ResultSet resultSet = null;
            
            try {
                resultSet = statement.executeQuery(sql);
            } catch (SQLException e) {
                System.out.println("Error get book data");
                e.printStackTrace();
            }
            
            if (resultSet != null) {
                try {
                    while (resultSet.next()) {
                        bookModel.setIsbn(resultSet.getInt(1));
                        bookModel.setTitle(resultSet.getString(2));
                        bookModel.setAuthor(resultSet.getString(3));
                        bookModel.setYear(resultSet.getString(4));
                        bookModel.setPrice(resultSet.getInt(5));
                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        
        return bookModel;
    }
    
    public int insertData(BookModel bookModel) {
        Statement statement = createStatement();
        int result = -1;
        
        if (statement != null) {
            String sql = "INSERT INTO `book` (`isbn`, `title`, `author`, `year`, `price`)"
                    + "VALUES (" + bookModel.getIsbn() + ", '" + bookModel.getTitle() + "', '" + bookModel.getAuthor() + "', '" + bookModel.getYear() + "', " + bookModel.getPrice() + ")";
            
            try {
                result = statement.executeUpdate(sql);
            } catch (SQLException e) {
                System.out.println("Can' insert new data");
                e.printStackTrace();
            }
        }
        
        return result;
    }
    
    public int updateData(int id, BookModel bookModel) {
        Statement statement = createStatement();
        int result = -1;
        
        if (statement != null) {
            String sql = "  UPDATE `book` SET" +
                            "`isbn` = " + bookModel.getIsbn() + "," +
                            "`title` = '" + bookModel.getTitle() + "'," +
                            "`author` = '" + bookModel.getAuthor() + "'," +
                            "`year` = '" + bookModel.getYear() +"'," +
                            "`price` = " + bookModel.getPrice() + " " +
                            "WHERE `isbn` = " + id ;
            
            try {
                result = statement.executeUpdate(sql);
            } catch (SQLException e) {
                System.out.println("Can' update new data");
                e.printStackTrace();
            }
        }
        
        return result;
    }
    
    public int deleteData(int isbn) {
        Statement statement = createStatement();
        int result = -1;
        
        if (statement != null) {
            String sql = "DELETE FROM book where isbn =" + isbn;
            
            try {
                result = statement.executeUpdate(sql);
            } catch (SQLException e) {
                System.out.println("Can't remove " + isbn);
                e.printStackTrace();
            }
        }
        
        return result;
    }
    
    public boolean closeConnection() {
        boolean isSuccess = true;
        
        try {
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
            
            isSuccess = false;
        }
        
        return isSuccess;
    }
    
    public Connection getConnection() {
        return connection;
    }
}
