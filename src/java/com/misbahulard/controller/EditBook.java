/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.misbahulard.controller;

import com.misbahulard.model.BookModel;
import com.misbahulard.model.DAOconnection;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author misba
 */
public class EditBook extends HttpServlet {

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        DAOconnection mySQL = new DAOconnection();
        BookModel book = new BookModel();
        boolean isSuccess = mySQL.createConnection();
        
        int isbn = Integer.parseInt(request.getParameter("isbn"));
        
        if (isSuccess) {
            book = mySQL.showDataByIsbn(isbn);
            request.setAttribute("book", book);
            
            RequestDispatcher rd = request.getRequestDispatcher("editBook.jsp");
            rd.forward(request, response);
        } else {
            RequestDispatcher rd = request.getRequestDispatcher("connection-error.jsp");
            rd.forward(request, response);
        }
        
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        DAOconnection mySQL = new DAOconnection();
        BookModel book = new BookModel();
        
        boolean isSuccess = mySQL.createConnection();
        int id = Integer.parseInt(request.getParameter("id"));
        int isbn = Integer.parseInt(request.getParameter("isbn"));
        String title = request.getParameter("title");
        String author = request.getParameter("author");
        String year = request.getParameter("year");
        int price = Integer.parseInt(request.getParameter("price"));
        
        book.setIsbn(isbn);
        book.setTitle(title);
        book.setAuthor(author);
        book.setYear(year);
        book.setPrice(price);
        
        if (isSuccess) {
            if (mySQL.updateData(id, book) == 1) {
                RequestDispatcher rd = request.getRequestDispatcher("index.jsp");
                rd.forward(request, response);
            }
        } else {
            RequestDispatcher rd = request.getRequestDispatcher("connection-error.jsp");
            rd.forward(request, response);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
